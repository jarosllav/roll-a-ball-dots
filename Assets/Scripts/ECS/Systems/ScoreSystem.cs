using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class ScoreSystem : SystemBase
{
    protected override void OnUpdate() {
        Entities
            .WithChangeFilter<ScoreComponent>()
            .ForEach((ScoreUI ui, in ScoreComponent score) => {
                ui.value.SetText(score.value.ToString());
            }).WithoutBurst().Run();
    }
}
