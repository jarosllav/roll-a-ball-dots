using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Mathematics;
using Unity.Transforms;

public class RotatingSystem : SystemBase
{
    protected override void OnUpdate() {
        float deltaTime = Time.DeltaTime;
        Entities
        .WithAll<PickableComponent>()
        .ForEach((ref Rotation rotation, in SpeedComponent speed) => {
            rotation.Value = math.mul(rotation.Value, quaternion.RotateY(math.radians(speed.value * deltaTime)));
        }).Schedule();
    }
}
