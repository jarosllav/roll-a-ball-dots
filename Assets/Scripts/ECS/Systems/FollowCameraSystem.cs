using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class FollowCameraSystem : SystemBase
{
    protected override void OnUpdate() {
        float deltaTime = Time.DeltaTime;

        Entity entity = GetSingletonEntity<CameraComponent>();
        FollowTargetComponent follow = GetComponent<FollowTargetComponent>(entity);
    
        Translation translation = GetComponent<Translation>(follow.target);
        UnityEngine.Camera.main.transform.position = translation.Value + follow.offset;
    }
}
