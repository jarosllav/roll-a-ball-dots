using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Rendering;

public class RainbowSystem : SystemBase
{
    float r = 255, g = 0, b = 0;

    protected override void OnUpdate() {
        if(r > 0 && b == 0){
            r--;
            g++;
        }
        if(g > 0 && r == 0){
            g--;
            b++;
        }
        if(b > 0 && g == 0){
            r++;
            b--;
        }

        float red = r / 255f, blue = b / 255f, green = g / 255f;

        Entities
            .WithAll<PlayerComponent>()
            .ForEach((ref MaterialColor color) => {
                color.Value = new float4(red, green, blue, 1);
            }).Run();
    }
}
