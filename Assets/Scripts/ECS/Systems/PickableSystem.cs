using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class PickableSystem : JobComponentSystem
{
    [BurstCompile]
    public struct TriggerJob : ITriggerEventsJob {
        public EntityCommandBuffer commandBuffer;
        public ComponentDataFromEntity<PickableComponent> pickableGroup;
        public ComponentDataFromEntity<ScoreComponent> scoreGroup;

        public void Execute(TriggerEvent triggerEvent) {
            if(this.pickableGroup.HasComponent(triggerEvent.EntityA)) {
                this.commandBuffer.DestroyEntity(triggerEvent.EntityA);
                this.increaseScore(triggerEvent.EntityB, 1);
            }
            else if(this.pickableGroup.HasComponent(triggerEvent.EntityB)) {
                this.commandBuffer.DestroyEntity(triggerEvent.EntityB);
                this.increaseScore(triggerEvent.EntityA, 1);
            }
        }

        private void increaseScore(Entity entity, int amount = 1) {
            if(this.scoreGroup.HasComponent(entity)) {
                ScoreComponent score = scoreGroup[entity];
                score.value = score.value + amount;
                scoreGroup[entity] = score;
            }
        }
    }

    private BuildPhysicsWorld physicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;
    private EntityCommandBufferSystem commandBufferSystem;

    protected override void OnCreate() {
        this.physicsWorld = this.World.GetOrCreateSystem<BuildPhysicsWorld>();
        this.stepPhysicsWorld = this.World.GetOrCreateSystem<StepPhysicsWorld>();
        this.commandBufferSystem = this.World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnStartRunning() {
        this.createPickableEntities();
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps) {
        EntityCommandBuffer commandBuffer = this.commandBufferSystem.CreateCommandBuffer();

        TriggerJob triggerJob = new TriggerJob {
            commandBuffer = commandBuffer,
            pickableGroup = GetComponentDataFromEntity<PickableComponent>(),
            scoreGroup = GetComponentDataFromEntity<ScoreComponent>()
        };

        JobHandle jobHandle = triggerJob.Schedule(this.stepPhysicsWorld.Simulation, ref this.physicsWorld.PhysicsWorld, inputDeps);
        this.commandBufferSystem.AddJobHandleForProducer(jobHandle);

        return jobHandle;
    }

    private void createPickableEntities() {
        SettingsComponent settings = GetSingleton<SettingsComponent>();

        for(int i = 0; i < settings.pickableCount; ++i) {
            float radians = math.radians(i * (360.0f / settings.pickableCount));
            float x = math.sin(radians) * settings.pickableRadius;
            float z = math.cos(radians) * settings.pickableRadius;
        
            Entity entity = World.EntityManager.Instantiate(settings.pickableEntity);
            World.EntityManager.SetComponentData(entity, new Translation {
                Value = new float3(x, 1f, z)
            });
        }
    }
}
