using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class MovementSystem : SystemBase
{
    protected override void OnUpdate() {
        float deltaTime = Time.DeltaTime;

        float2 input = new float2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        Entities
        .WithAll<PlayerComponent>()
        .ForEach((ref PhysicsVelocity velocity, in SpeedComponent speed) => {
            float2 linear = velocity.Linear.xz;
            linear += input * speed.value * deltaTime;

            velocity.Linear.xz = linear;
        }).Schedule();
    }
}
