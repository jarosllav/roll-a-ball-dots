using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable, GenerateAuthoringComponent]
public class ScoreUI : IComponentData
{
    public TMPro.TextMeshProUGUI value;
}
