using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
public struct SettingsComponent : IComponentData
{
    public Entity pickableEntity;
    public float pickableRadius;
    public int pickableCount;
}
