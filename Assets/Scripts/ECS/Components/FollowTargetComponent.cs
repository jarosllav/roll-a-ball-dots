using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable, GenerateAuthoringComponent]
public struct FollowTargetComponent : IComponentData
{
    public Entity target;
    public float3 offset;
}
