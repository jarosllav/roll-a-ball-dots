using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[DisallowMultipleComponent]
public class CameraConverter : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] private Transform target;
    [SerializeField] private float3 offset;
    [SerializeField] private float speed;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        dstManager.AddComponentData(entity, new CameraComponent {});
        dstManager.AddComponentData(entity, new SpeedComponent { value = this.speed });
        dstManager.AddComponentData(entity, new FollowTargetComponent { 
            target = conversionSystem.GetPrimaryEntity(this.target.gameObject),
            offset = this.offset
        });
    }
}
