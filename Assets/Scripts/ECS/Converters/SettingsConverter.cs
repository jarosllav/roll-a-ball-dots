using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[DisallowMultipleComponent]
public class SettingsConverter : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
{
    [SerializeField] private GameObject pickablePrefab;
    [SerializeField] private float pickableRadius;
    [SerializeField] private int pickableCount;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        dstManager.AddComponentData(entity, new SettingsComponent {
            pickableEntity = conversionSystem.GetPrimaryEntity(this.pickablePrefab),
            pickableRadius = this.pickableRadius,
            pickableCount = this.pickableCount
        });
    }

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs) {
        referencedPrefabs.Add(this.pickablePrefab);
    }
}
